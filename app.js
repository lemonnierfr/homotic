var server = require('./server');
var static_server = require('./static_server')
var dht_sensors = require('./DHT11');
var requestHandlers = require("./request_handlers");
var router = require('./router');
var moduleHandlers = require('./modules');
var arduino = require('./arduino.js');

var handle = {};
handle['/'] = requestHandlers.start;
handle['/Probes'] = requestHandlers.Probes;
handle['/Modules'] = requestHandlers.Modules;
handle['/RecordMod'] = requestHandlers.RecordMod;
handle['/GetMod'] = requestHandlers.GetMod;
handle['/GetArd'] = requestHandlers.GetArd;

var modules = {};
modules[0] = {"Name" : "DHT11 Sensor", "Description" : "Provides Temperature and Humidity", "Fields" : [{"Name":"Port Number"}]};
modules[1] = {"Name" : "433 Receiver", "Description" : "Allow to receive code from 433 remote control", "Fields" : [{"Name":"Port Number"},{"Name":"Port2 Number"}]};
modules[2] = {"Name" : "433 Transmetter", "Description" : "Allow to send code to 433 devices", "Fields" : [{"Name":"Port Number"},{"Name":"Port2 Number"}]};

server.start(router.route, handle, modules);