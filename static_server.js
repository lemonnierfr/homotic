// Server serving static HTTP requests (GET only)
var static = require('node-static');
var http = require('http');
var file = new static.Server('./client');

function servepublic(req, res) {
    file.serve(req, res, function (err, result) {
        if (err) {  // There was an error serving the file
            console.log("Error serving " + req.url + " - " + err.message);
                    // Respond to the client
            res.writeHead(404, {"Content-Type": "text/html"});
            res.write("404 Not found");
            res.end();
        }
    });
}

exports.servepublic = servepublic;