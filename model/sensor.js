var mongoose = require( 'mongoose' );

var sensorSchema = new mongoose.Schema({
    Item : String,
    sensorType : Number,
    Settings : [{PortName : String, GPIO : Number}],
    date: { type: Date, default: Date.now }
});

var sensor = module.exports = mongoose.model('sensor', sensorSchema);