var mongoose = require( 'mongoose' );
var dbURI = 'mongodb://localhost/homotic';
mongoose.connect(dbURI);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open to ' + dbURI);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

require('./../model/sensor');

/*
function GetSensors()
{
    connectDB();
    var query = sensorModel.find({});
    var data = [];
    query.where('Item','sensor');
    query.limit(10);
//    query.exec(function (err, comms) {
//        if (err) console.log(err.message); else console.log('Request OK');
//        for (var i = 0, l = comms.length; i < l; i++) {
//            comm = comms[i];
//            console.log('------------------------------');
//            console.log('sensorType : ' + comm.sensorType);
//            console.log('Fields : ' + comm.Settings);
//            console.log('Date : ' + comm.date);
//            console.log('ID : ' + comm._id);
//            console.log('------------------------------');
//        }
//        data = comms;
//        console.log(data);
//    });
    mongoose.connection.close();
    return data;
}

function AddSensor(data)
{
    connectDB();
    var datajson = JSON.parse(data);
    var mySensor = new sensorModel();
    mySensor.Item = 'sensor';
    mySensor.sensorType = datajson.sensorType;
    mySensor.Settings = datajson.Fields;
    mySensor.save(function (err) {
        if (err) { throw err; }
        console.log('Sensor Added !');
    mongoose.connection.close();
    });
}

exports.AddSensor = AddSensor;
exports.GetSensors = GetSensors;
*/