var db = require('./model/database');
var mongoose = require( 'mongoose' ),
    sensor = mongoose.model('sensor');

function GetList(modules) {
    return JSON.stringify(modules);
}

function RecordMod(data) {
    var datajson = JSON.parse(data);
    DB.AddSensor(datajson);
}


function GetMod (callback) {
    var query = sensor.find({});
    query.where('Item','sensor');
    query.limit(10);
    query.exec(function (err, comms) {
        if (err) console.log(err.message);
/*        for (var i = 0, l = comms.length; i < l; i++) {
            comm = comms[i];
            console.log('------------------------------');
            console.log('sensorType : ' + comm.sensorType);
            console.log('Fields : ' + comm.Settings);
            console.log('Date : ' + comm.date);
            console.log('ID : ' + comm._id);
            console.log('------------------------------');
        }
*/        data = comms;
        callback(data);
    });
}

exports.GetList = GetList;
exports.RecordMod = RecordMod;
exports.GetMod = GetMod;