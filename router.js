var requestHandlers = require("./request_handlers");

function route(handle, pathname, response, request, modules) {
    // Route for Static file
    switch(pathname) {
        case '/GetMod':
            handle['/GetMod'](response);
            break;
        case '/RecordMod':
            handle['/RecordMod'](request, response);
            break;
        case '/Probes':
            handle['/Probes'](response);
            break;
        case '/Modules':
            handle['/Modules'](response, modules);
            break;
        case '/GetArd':
            handle['/GetArd'](response);
            break;
        default:
            handle['/'](response, request);
    }
}

exports.route = route;