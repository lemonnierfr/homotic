// Server serving dynamic HTTP requests (GET & POST)
var http = require('http');
var url = require('url');

function start(route,handle, modules) {
    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;
        route(handle, pathname, response, request, modules);
    }

  http.createServer(onRequest).listen(8888);
  console.log("Server has started."); 
}

exports.start = start;