var sensorLib = require('node-dht-sensor');

function GetVal() {
    var dht_sensor = {
        initialize: function () {
            return sensorLib.initialize(11, 4);
        },
        readTemp: function () {
            var readout = sensorLib.read();
            return readout.temperature.toFixed(2);
        },    
        readHumi: function () {
            var readout = sensorLib.read();
            return readout.humidity.toFixed(2);
        }
    };
    
    if (dht_sensor.initialize()) {
        console.log('DHT sensor init OK');
        var Val = {
            Temp : dht_sensor.readTemp(),
            Humi : dht_sensor.readHumi()
            };
        return JSON.stringify(Val);
    } else {
        console.log('Failed to initialize sensor');
    }
}

exports.GetVal = GetVal;