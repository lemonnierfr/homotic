var mongoose = require('mongoose');
var sensorSchema = new mongoose.Schema({
    Item : String,
    sensorType : Number,
    Settings : [{PortName : String, GPIO : Number}],
    date: { type: Date, default: Date.now }
});
var sensorModel = mongoose.model('sensor', sensorSchema);


function connectDB()
{
    mongoose.connect('mongodb://localhost/homotic');
}

function GetSensors()
{
    connectDB();
    var query = sensorModel.find({});
    var data = [];
    query.where('Item','sensor');
    query.limit(10);
//    query.exec(function (err, comms) {
//        if (err) console.log(err.message); else console.log('Request OK');
//        for (var i = 0, l = comms.length; i < l; i++) {
//            comm = comms[i];
//            console.log('------------------------------');
//            console.log('sensorType : ' + comm.sensorType);
//            console.log('Fields : ' + comm.Settings);
//            console.log('Date : ' + comm.date);
//            console.log('ID : ' + comm._id);
//            console.log('------------------------------');
//        }
//        data = comms;
//        console.log(data);
//    });
    mongoose.connection.close();
    return data;
}

function AddSensor(data)
{
    connectDB();
    var datajson = JSON.parse(data);
    var mySensor = new sensorModel();
    mySensor.Item = 'sensor';
    mySensor.sensorType = datajson.sensorType;
    mySensor.Settings = datajson.Fields;
    mySensor.save(function (err) {
        if (err) { throw err; }
        console.log('Sensor Added !');
    mongoose.connection.close();
    });
}

exports.AddSensor = AddSensor;
exports.GetSensors = GetSensors;