~~~~
          _______  _______  _______ __________________ _______ 
|\     /|(  ___  )(       )(  ___  )\__   __/\__   __/(  ____ \
| )   ( || (   ) || () () || (   ) |   ) (      ) (   | (    \/
| (___) || |   | || || || || |   | |   | |      | |   | |      
|  ___  || |   | || |(_)| || |   | |   | |      | |   | |      
| (   ) || |   | || |   | || |   | |   | |      | |   | |      
| )   ( || (___) || )   ( || (___) |   | |   ___) (___| (____/\
|/     \|(_______)|/     \|(_______)   )_(   \_______/(_______/
--------------------------------------------------------------- 
~~~~

Welcome to Homotic server
Control and interact with sensors from Raspberry/Arduino/Piface/...
From beginner to beginners oO


### TODO
* Control LCD 20x4 display
* Control 433MHz transmitter/receiver
* Recording of DTH11 and graph display
* Recording of 433 receiver sequence
* Implementation of JARVIS
* Core nodejs
* Implement Client modules
* Module management interface
* Arduino integration
* SECURITY !!!!
* Multi-user
* ACK message when new sensor successfully added
* Get Raspberry info (CPU, MEM)
* Implementation of callback functions

### BUGS (not blocking)
* Dashboard display
* Sensor settings not displayed for default one

### DONE
* _Convert to modules --- 23/11/2014_
* _Get DTH 11 feedback --- 24/11/2014_
* _Arduino code to manage DHT11 and 433 receiver --- 30/11/2014_ 
* _Access to mongodb/mongoose --- 1/12/2014_

(c) Frédéric LE MONNIER