var static = require ('./static_server');
var arduino = require('./arduino.js');
var DHT = require('./DHT11');
var Mod = require('./modules.js');
var sensormodules = require('./modules');
var querystring = require("querystring"),
    fs = require("fs"),
    formidable = require("formidable");

function start(response, request) {
    // Serve static files
    static.servepublic(request, response);
}

function upload(response, request) {
  console.log("Request handler 'upload' was called.");

  var form = new formidable.IncomingForm();
  console.log("about to parse");
  form.parse(request, function(error, fields, files) {
    console.log("parsing done");

    /* Possible error on Windows systems:
       tried to rename to an already existing file */
    fs.rename(files.upload.path, "/tmp/test.png", function(err) {
      if (err) {
        fs.unlink("/tmp/test.png");
        fs.rename(files.upload.path, "/tmp/test.png");
      }
    });
    response.writeHead(200, {"Content-Type": "text/html"});
    response.write("received image:<br/>");
    response.write("<img src='/show' />");
    response.end();
  });
}

function show(response) {
  console.log("Request handler 'show' was called.");
  response.writeHead(200, {"Content-Type": "image/png"});
  fs.createReadStream("/tmp/test.png").pipe(response);
}

function Probes(response) {
    response.writeHead(200, {'Content-Type': 'application/json'});
    response.write(DHT.GetVal());
    response.end();
}

function Modules(response, mod) {
    response.writeHead(200, {'Content-Type': 'application/json'});
    response.write(sensormodules.GetList(mod));
    response.end();
}

function RecordMod(request, response) {
    request.on('data', function(chunk) {
      Mod.RecordMod(chunk.toString());
    });
    response.writeHead(200, {"Content-Type": "text/html"});
    response.write("OK");
    response.end();
}

function GetMod(response) {
    sensormodules.GetMod(function (data) {
        console.log(data);
        return data;
    });
//    response.writeHead(200, {'Content-Type': 'application/json'});
//        response.write(JSON.stringify(datajson));
//        response.end();
}

function GetArd(response) {
    response.writeHead(200, {'Content-Type': 'application/json'});
    var data = arduino.GetArd();
    response.write(data);
    response.end();
}

exports.start = start;
exports.Probes = Probes;
exports.Modules = Modules;
exports.RecordMod = RecordMod;
exports.GetMod = GetMod;
exports.GetArd = GetArd;